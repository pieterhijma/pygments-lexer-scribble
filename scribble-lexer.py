# SPDX-FileCopyrightText: 2023 Pieter Hijma <info@pieterhijma.net>
#
# SPDX-License-Identifier: BSD-2-Clause

from pygments.lexer import RegexLexer
from pygments.token import *

class CustomLexer(RegexLexer):
    """Custom Lexer for Scribble"""
    name = 'Scribble'
    aliases = ['scribble']
    filenames = ['*.scrbl']

    # More complex with states
    # Currently not necessary for my usecase
    
    # tokens = {
    #     'root': [
    #         (r'@[a-zA-Z-]+', Name.Builtin, 'tag'),
    #         (r' .*\n', Text),
    #         (r'.*\n', Text),
    #     ],
    #     'tag': [
    #         (r'\[', Text, 'params'),
    #         (r'{', Text, 'root'),
    #         (r'}', Text, '#pop')
    #     ],
    #     'params': [
    #         (r':[a-z]+', Keyword),
    #         (r'".*"', String),
    #         (r'\]', Text, '#pop'),
    #     ]
    # }

    tokens = {
        'root': [
            (r'@[a-zA-Z-]+', Name.Builtin),
            (r'".*"', String),
            (r'[^@]', Text),
            (r'@;.*\n', Comment.Single),
        ]
    }

