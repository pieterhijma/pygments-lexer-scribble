<!--
SPDX-FileCopyrightText: 2023 Pieter Hijma <info@pieterhijma.net>

SPDX-License-Identifier: BSD-2-Clause
-->

# Pygments Lexer for Scribble

## Introduction

This project contains a simple [Pygments](#) lexer for [Scribble
files](https://docs.racket-lang.org/scribble/index.html).

## License

This project is licensed under the same license as Pygments, a BSD 2-clause
license.

## Usage

For formatting the file `input.scrbl`:

``` sh
pygmentize -x -l scribble-lexer.py input.scrbl
```


## Debugging

To debug the tokens on a file `input.scrbl`, execute the following and open the
file `index.html` in a browser.

``` sh
pygmentize -x -f html -Ofull,debug_token_types -l scribble-lexer.py input.scrbl  > index.html
```
